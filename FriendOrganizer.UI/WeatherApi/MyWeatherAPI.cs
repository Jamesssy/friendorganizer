﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows.Documents;
using FriendOrganizer.Model;
using Newtonsoft.Json;

namespace FriendOrganizer.UI.WeatherApi
{
    public enum WeatherLocation
    {
        Gothenburg,
        Stockholm
    }

    public static class MyWeatherAPI
    {
        private const string ImageUrlFormat = "https://www.metaweather.com/static/img/weather/png/64/{0}.png";
        private static readonly HttpClient HttpClient = new HttpClient();

        static MyWeatherAPI()
        {
            HttpClient.BaseAddress = new Uri("https://www.metaweather.com/api/");
            HttpClient.DefaultRequestHeaders.Accept.Clear();
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private static async Task<int> GetLocationId(WeatherLocation location)
        {
            var json = await HttpClient.GetStringAsync($"location/search/?query={location.ToString().ToLower()}");
            var m = JsonConvert.DeserializeObject<List<TempWoeidModel>>(json);
            int woeId = m[0].woeid;
            return woeId;
        }

        //public static void Run(string location)
        //{
        //    //location = "gothenburg";
        //    RunAsync(location);
        //}

        //static async Task RunAsync(string location)
        //{ 
        //    // New code:
        //    //httpClient.BaseAddress = new Uri("https://www.metaweather.com/api/");
        //    //httpClient.DefaultRequestHeaders.Accept.Clear();
        //    //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //    //var json = await HttpClient.GetStringAsync($"location/search/?query={location}");
        //    //var m = JsonConvert.DeserializeObject<List<TempWoeidModel>>(json);

            

        //    //int woeId = m[0].woeid;

        //    try
        //    {
        //        // Create a new product
        //        //Consolidated_Weather weather = null;


        //        // Get the product
        //        //weather = await GetWeatherAsync($"location/{woeId}/");

        //        //ShowProduct(weather);

        //        //// Update the product
        //        //Console.WriteLine("Updating price...");
        //        //weather.Price = 80;
        //        //await UpdateProductAsync(weather);

        //        //// Get the updated product
        //        //weather = await GetWeatherAsync(url.PathAndQuery);
        //        //ShowProduct(weather);

        //        //// Delete the product
        //        //var statusCode = await DeleteProductAsync(weather.Id);
        //        //Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");

        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.Message);
        //    }



        //    Console.ReadLine();
        //}

        public static async Task<Consolidated_Weather> GetWeatherAsync(WeatherLocation location, DateTime date)
        {
            Consolidated_Weather consolidatedWeather = new Consolidated_Weather
            {
                ErrorMessage = "The date for a weather report is to far away!",
                ImageUrl = ""
            };
            try
            {
                int locationId = await GetLocationId(location);
                string path = $"location/{locationId}/{date.Year}/{date.Month}/{date.Day}/";
                HttpResponseMessage response = await HttpClient.GetAsync(path);
                if (response.IsSuccessStatusCode)
                {
                    var consolidatedWeathers =
                        await response.Content.ReadAsAsync<IEnumerable<Consolidated_Weather>>();
                    consolidatedWeather = consolidatedWeathers.First();
                    consolidatedWeather.ImageUrl =
                        string.Format(ImageUrlFormat, consolidatedWeather.weather_state_abbr);
                }
            }
            catch (Exception exception)
            {
                // TODO: Handle exception?
            }
            return consolidatedWeather;
        }

        //static void ShowProduct(Weather weather)
        //{
        //    Console.WriteLine($"Name: {weather.title}\tPrice: {weather.time}\tCategory: {weather.consolidated_weather[0].weather_state_name}");
        //}

        //static async Task<Uri> CreateProductAsync(Weather weather)
        //{
        //    var location = "gothenburg";

        //    HttpResponseMessage response = await httpClient.PostAsJsonAsync("/api/location/search/?query="+location, weather);
        //    response.EnsureSuccessStatusCode();

        //    // return URI of the created resource.
        //    return response.Headers.Location;
        //}



        //static async Task<Weather> UpdateProductAsync(Weather weather)
        //{
        //    HttpResponseMessage response = await httpClient.PutAsJsonAsync($"api/products/{weather.Id}", weather);
        //    response.EnsureSuccessStatusCode();

        //    // Deserialize the updated product from the response body.
        //    weather = await response.Content.ReadAsAsync<Weather>();
        //    return weather;
        //}

        //static async Task<HttpStatusCode> DeleteProductAsync(string id)
        //{
        //    HttpResponseMessage response = await httpClient.DeleteAsync($"api/products/{id}");
        //    return response.StatusCode;
        //}


    }
}