﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FriendOrganizer.UI.WeatherApi;

namespace FriendOrganizer.UI.ViewModel
{
    public class WeatherDetailViewModel
    {
        public Consolidated_Weather ConsolidatedWeather { get; set; }
    }
}
