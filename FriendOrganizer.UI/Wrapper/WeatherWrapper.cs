﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FriendOrganizer.UI.WeatherApi;

namespace FriendOrganizer.UI.Wrapper
{
    public class WeatherWrapper : ModelWrapper<Consolidated_Weather>
    {
        public WeatherWrapper(Consolidated_Weather model) : base(model)
        {

        }

        public string ImageUrl
        {
            get => GetValue<string>();
            set => SetValue(value);
        }

        public string weather_state_name
        {
            get => GetValue<string>();
            set => SetValue(value);
        }

        public string wind_direction_compass
        {
            get => GetValue<string>();
            set => SetValue(value);
        }


        public float min_temp
        {
            get => GetValue<float>();
            set => SetValue(value);
        }
        public float max_temp
        {
            get => GetValue<float>();
            set => SetValue(value);
        }
        public float the_temp
        {
            get => GetValue<float>();
            set => SetValue(value);
        }
        public float wind_speed
        {
            get => GetValue<float>();
            set => SetValue(value);
        }

       


    }
}
