﻿using FriendOrganizer.Model;
using System;
using FriendOrganizer.UI.WeatherApi;

namespace FriendOrganizer.UI.Wrapper
{
    public class MeetingWrapper : ModelWrapper<Meeting>
    {
        public MeetingWrapper(Meeting model) : base(model)
        {
        }

        public int Id { get { return Model.Id; } }

        public string Title
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }

        public WeatherLocation Location
        {
            get
            {
                object obj = Enum.Parse(typeof(WeatherLocation), Model.Location is null || Model.Location == "" ? WeatherLocation.Gothenburg.ToString() : Model.Location, true);
                return (WeatherLocation) obj;
            }
            set { Model.Location = value.ToString(); OnPropertyChanged(); }
        }

        public DateTime DateFrom
        {
            get { return GetValue<DateTime>(); }
            set
            {
                SetValue(value);
                if (DateTo < DateFrom)
                {
                    DateTo = DateFrom;
                }
            }
        }

        public DateTime DateTo
        {
            get { return GetValue<DateTime>(); }
            set
            {
                SetValue(value);
                if (DateTo < DateFrom)
                {
                    DateFrom = DateTo;
                }
            }
        }
    }
}
